"""
Application permettant de rajouter des commentaires sur des films via un compte
"""
import uuid
import datetime
import json
from flask import Flask, request, jsonify

error_token = {'error': 'Not token'}
error_film = {'error': 'Not film'}
error_comment = {'error': 'Not comment'}
error_json = {'error': 'Not json'}
error_id = {'error': 'Not bad id'}
error_password = {'error': 'Not password'}
error_user = {'error': 'Not user'}
error_user_password = {'error': 'Bad user or password'}

def write_json():
    """
    Écrit les données dans un fichier json
    """
    with open('data.json', 'w') as file_json:
        json.dump(data_flask, file_json)

def read_json():
    """
    Lit les données dans un fichier json
    """
    with open('data.json', 'r') as file_json:
        data_json = json.load(file_json)
        return data_json

data_flask = read_json()
if data_flask is None:
    data_flask = {'users': {}, 'comments': {}}
users = data_flask['users']
comments = data_flask['comments']

app = Flask(__name__)

@app.teardown_request
def teardown_request(exception=None):
    """
    Fonction qui est appeller à chaque fois qu'une requête est effectué
    Et permet d'écrire les donnes dans un fichier
    """
    write_json()

@app.route('/', methods=['GET'])
def hello_world():
    """
    Hello Wolrd
    """
    return 'Hello World'

@app.route('/register', methods=['POST'])
def add_account():
    """
    Permet de créer un compte
    """
    if not request.is_json:
        return jsonify(error_json), 400
    else:
        content = request.get_json()
    if not content.get('user', None):
        return jsonify(error_user), 400
    if not content.get('password', None):
        return jsonify(error_password), 400

    users[content['user']] = {
        'password': content['password'],
        'token': str(uuid.uuid4())
    }
    return jsonify({'token' : users[content['user']]['token']}), 200

@app.route('/login', methods=['POST'])
def login():
    """
    Permet de s'indentifier et recupérer un token
    """
    if not request.is_json:
        return jsonify(error_json), 400
    else:
        content = request.get_json()
    if not content.get('user', None):
        return jsonify(error_user), 400
    if not content.get('password', None):
        return jsonify(error_password), 400

    if not content['user'] in users:
        return jsonify(error_user_password), 400

    if not content['password'] == users[content['user']]['password']:
        return jsonify(error_user_password), 400

    return jsonify({'token' : users[content['user']]['token']})

@app.route('/comment', methods=['POST'])
def create_comment():
    """
    Créer un commentaire
    """
    if not request.is_json:
        return jsonify(error_json), 400
    else:
        content = request.get_json()
    if not content.get('token', None):
        return jsonify(error_token), 401
    if not content.get('film', None):
        return jsonify(error_film), 400
    if not content.get('comment', None):
        return jsonify(error_comment), 400

    username = None
    for user in users:
        if content['token'] == users[user]['token']:
            username = user
            break
        else:
            username = None
    if username is None:
        return jsonify(error_token), 401
    else:
        id_comment = str(uuid.uuid4())
        comments[id_comment] = {
            'film': content['film'],
            'comment': content['comment'],
            'user': username,
            'date': str(datetime.datetime.today()),
            'id': id_comment
        }
        return jsonify(comments[id_comment])

@app.route('/comment', methods=['GET'])
def get_comment():
    """
    Récupère tout les commentaires
    """
    return jsonify(comments)

@app.route('/comment/<id_comment>', methods=['GET'])
def get_comment_id(id_comment):
    """
    Permet de récupérer un commentaire par son id
    """
    if not id_comment in comments:
        return jsonify(error_id), 400
    else:
        return jsonify(comments[id_comment])

@app.route('/comment/<id_comment>', methods=['PUT'])
def update_comment_id(id_comment):
    """
    Met à jour un commentaire par son id
    """
    if not request.is_json:
        return jsonify(error_json), 400
    else:
        content = request.get_json()
    if not content.get('token', None):
        return jsonify(error_token), 401
    if not content.get('film', None):
        return jsonify(error_film), 400
    if not content.get('comment', None):
        return jsonify(error_comment), 400
    if not id_comment in comments:
        return jsonify(error_id), 400

    username = None
    for user in users:
        if content['token'] == users[user]['token'] and user == comments[id_comment]['user']:
            username = user
            break
        else:
            username = None
    if username is None:
        return jsonify(error_token), 401
    else:
        comments[id_comment] = {
            'film': content['film'],
            'comment': content['comment'],
            'user': username,
            'date': str(datetime.datetime.today()),
            'id': id_comment
        }
        return jsonify(comments[id_comment])

@app.route('/comment/<id_comment>', methods=['DELETE'])
def delete_comment_id(id_comment):
    """
    Supprime un commentaire par son id
    """
    if not request.is_json:
        return jsonify(error_json), 400
    else:
        content = request.get_json()
    if not content.get('token', None):
        return jsonify(error_token), 401
    if not id_comment in comments:
        return jsonify(error_id), 400

    username = None
    for user in users:
        if content['token'] == users[user]['token'] and user == comments[id_comment]['user']:
            username = user
            break
        else:
            username = None
    if username is None:
        return jsonify(error_token), 401
    else:
        comment = comments[id_comment]
        del comments[id_comment]
        return jsonify(comment)

if __name__ == '__main__':
    app.run()
