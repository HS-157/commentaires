"""
Tests de l'application commentaire
"""
import unittest
import json
import random
import uuid
import main

films = [
    'Ex Machina',
    'Les Nouveaux Héros',
    'Terminator',
    'Matrix',
    'Gran Torino'
]

comments = [
    'Bonne critique',
    'Excellente critique',
    'Primé aux Oscar de nombreuse fois !',
    'C tro b1 !',
    "J'adore Robert Downey Jr !"
]

users = {
    'Test1': {
        'password': '123456',
        'token': None,
        'comments': []
    },
    'Test2': {
        'password': '12fdsfsd6',
        'token': None,
        'comments': []
    },
    'Test3': {
        'password': '1fds2fsd',
        'token': None,
        'comments': []
    }
}

error_token = {'error': 'Not token'}
error_film = {'error': 'Not film'}
error_comment = {'error': 'Not comment'}
error_json = {'error': 'Not json'}
error_id = {'error': 'Not bad id'}
error_password = {'error': 'Not password'}
error_user = {'error': 'Not user'}
error_user_password = {'error': 'Bad user or password'}

class comment_test(unittest.TestCase):

    def setUp(self):
        """
        Fonction mettant l'application en mode test
        """
        main.app.testing = True
        self.app = main.app.test_client()

    def test0_hello_world(self):
        """
        Test Hello World
        """
        r = self.app.get('/')
        assert r.status_code == 200
        assert b'Hello World' == r.data

    def test1_register(self):
        """
        Test création d'utilisateur
        """
        for user, datas in users.items():
            r = self.app.post(
                '/register',
                data=json.dumps({"user": user, "password": datas['password']}),
                mimetype='application/json'
            )
            assert r.status_code == 200
            r_json = json.loads(r.data.decode('utf8'))
            assert 'token' in r_json
            users[user]['token'] = r_json['token']

    def test2_login(self):
        """
        Test d'authentification
        """
        for user, datas in users.items():
            r = self.app.post(
                '/login',
                data=json.dumps({"user": user, "password": datas['password']}),
                mimetype='application/json'
            )
            assert r.status_code == 200
            r_json = json.loads(r.data.decode('utf8'))
            assert 'token' in r_json
            assert r_json['token'] == users[user]['token']

    def test3_create_comment(self):
        """
        Test la création de commentaire
        """
        for user, datas in users.items():
            film = random.choice(films)
            comment = random.choice(comments)
            r = self.app.post(
                '/comment',
                data=json.dumps({"token": datas['token'], "film": film, "comment": comment}),
                mimetype='application/json'
            )
            assert r.status_code == 200
            r_json = json.loads(r.data.decode('utf8'))
            assert 'film' in r_json
            assert 'comment' in r_json
            assert 'user' in r_json
            assert 'date' in r_json
            assert 'id' in r_json
            assert film == r_json['film']
            assert comment == r_json['comment']
            assert user == r_json['user']
            users[user]['comments'].append(r_json)

    def test4_get_comment(self):
        """
        Test de récupération de tout les commentaires
        """
        r = self.app.get('/comment')
        assert r.status_code == 200
        r_json = json.loads(r.data.decode('utf8'))
        for user, datas in users.items():
            for comment in datas['comments']:
                assert 'film' in r_json[comment['id']]
                assert 'comment' in r_json[comment['id']]
                assert 'user' in r_json[comment['id']]
                assert 'date' in r_json[comment['id']]
                assert 'id' in r_json[comment['id']]
                assert comment['film'] == r_json[comment['id']]['film']
                assert comment['comment'] == r_json[comment['id']]['comment']
                assert comment['user'] == r_json[comment['id']]['user']
                assert comment['date'] == r_json[comment['id']]['date']
                assert comment['id'] == r_json[comment['id']]['id']

    def test5_get_comment_id(self):
        """
        Test de récupération d'un commentaire par id
        """
        for user, datas in users.items():
            for comment in datas['comments']:
                r = self.app.get('/comment/%s' % comment['id'])
                assert r.status_code == 200
                r_json = json.loads(r.data.decode('utf8'))
                assert 'film' in r_json
                assert 'comment' in r_json
                assert 'user' in r_json
                assert 'date' in r_json
                assert 'id' in r_json
                assert comment['film'] == r_json['film']
                assert comment['comment'] == r_json['comment']
                assert comment['user'] == r_json['user']
                assert comment['date'] == r_json['date']
                assert comment['id'] == r_json['id']
                assert comment == r_json

    def test6_update_comment_id(self):
        """
        Test de modification d'un commentaire par id
        """
        for user, datas in users.items():
            for comment in datas['comments']:
                comment_str = random.choice(comments)
                r = self.app.put(
                    '/comment/%s' % comment['id'],
                    data=json.dumps(
                        {
                            "token": datas['token'],
                            "film": comment['film'],
                            "comment": comment_str
                        }
                    ),
                    mimetype='application/json')
                assert r.status_code == 200
                r_json = json.loads(r.data.decode('utf8'))
                assert 'film' in r_json
                assert 'comment' in r_json
                assert 'user' in r_json
                assert 'date' in r_json
                assert 'id' in r_json
                assert comment['film'] == r_json['film']
                assert comment_str == r_json['comment']
                assert user == r_json['user']
                assert comment['id'] == r_json['id']
                assert comment['date'] != r_json['date']
                users[user]['comments'].remove(comment)
                users[user]['comments'].append(r_json)

    def test7_delete_comment_id(self):
        """
        Test de suppréssion d'un commentaire par id
        """
        for user, datas in users.items():
            for comment in datas['comments']:
                r = self.app.delete(
                    '/comment/%s' % comment['id'],
                    data=json.dumps({"token": datas['token']}),
                    mimetype='application/json'
                )
                assert r.status_code == 200
                r_json = json.loads(r.data.decode('utf8'))
                assert comment == r_json
                users[user]['comments'].remove(comment)

    def test8_add_account_err(self):
        """
        Test d'erreurs sur la création d'un compte
        """
        r = self.app.post('/register', data=json.dumps({"user": 'test', "password": 'password'}))
        assert r.status_code == 400
        r_json = json.loads(r.data.decode('utf8'))
        assert error_json == r_json

        r = self.app.post(
            '/register',
            data=json.dumps({"password": 'password'}),
            mimetype='application/json'
        )
        assert r.status_code == 400
        r_json = json.loads(r.data.decode('utf8'))
        assert error_user == r_json

        r = self.app.post(
            '/register',
            data=json.dumps({"user": 'test'}),
            mimetype='application/json'
        )
        assert r.status_code == 400
        r_json = json.loads(r.data.decode('utf8'))
        assert error_password == r_json

    def test9_login_err(self):
        """
        Test d'erreurs sur l'authentification
        """
        r = self.app.post(
            '/login',
            data=json.dumps({"user": 'test', "password": 'password'})
        )
        assert r.status_code == 400
        r_json = json.loads(r.data.decode('utf8'))
        assert error_json == r_json

        r = self.app.post(
            '/login',
            data=json.dumps({"password": 'password'}),
            mimetype='application/json'
        )
        assert r.status_code == 400
        r_json = json.loads(r.data.decode('utf8'))
        assert error_user == r_json

        r = self.app.post(
            '/login',
            data=json.dumps({"user": 'test'}),
            mimetype='application/json'
        )
        assert r.status_code == 400
        r_json = json.loads(r.data.decode('utf8'))
        assert error_password == r_json

        r = self.app.post(
            '/login',
            data=json.dumps({"user": str(uuid.uuid4()), "password": 'password'}),
            mimetype='application/json'
        )
        assert r.status_code == 400
        r_json = json.loads(r.data.decode('utf8'))
        assert error_user_password == r_json

        r = self.app.post(
            '/login',
            data=json.dumps({"user": 'Test1', "password": str(uuid.uuid4())}),
            mimetype='application/json'
        )
        assert r.status_code == 400
        r_json = json.loads(r.data.decode('utf8'))
        assert error_user_password == r_json

    def test10_create_comment_err(self):
        """
        Test d'erreur sur la création de commentaire
        """
        r = self.app.post(
            '/comment',
            data=json.dumps({"token": users['Test1']['token'], "film": 'test', "comment": 'test'})
        )
        assert r.status_code == 400
        r_json = json.loads(r.data.decode('utf8'))
        assert error_json == r_json

        r = self.app.post(
            '/comment',
            data=json.dumps({"film": 'test', "comment": 'test'}),
            mimetype='application/json'
        )
        assert r.status_code == 401
        r_json = json.loads(r.data.decode('utf8'))
        assert error_token == r_json

        r = self.app.post(
            '/register',
            data=json.dumps({"user": 'test', "password": 'test'}),
            mimetype='application/json'
        )
        assert r.status_code == 200
        r_json = json.loads(r.data.decode('utf8'))
        token = r_json['token']

        r = self.app.post(
            '/comment',
            data=json.dumps({"token": token, "comment": 'test'}),
            mimetype='application/json'
        )
        assert r.status_code == 400
        r_json = json.loads(r.data.decode('utf8'))
        assert error_film == r_json

        r = self.app.post(
            '/comment',
            data=json.dumps({"token": token, "film": 'test'}),
            mimetype='application/json'
        )
        assert r.status_code == 400
        r_json = json.loads(r.data.decode('utf8'))
        assert error_comment == r_json

        r = self.app.post(
            '/comment',
            data=json.dumps({"token": str(uuid.uuid4()), "film": 'test', "comment": 'test'}),
            mimetype='application/json'
        )
        assert r.status_code == 401
        r_json = json.loads(r.data.decode('utf8'))
        assert error_token == r_json

    def test11_get_comment_id_err(self):
        """
        Test d'erreur sur la récupération de commentaire sur l'id
        """
        r = self.app.get('/comment/%s' % str(uuid.uuid4()))
        assert r.status_code == 400
        r_json = json.loads(r.data.decode('utf8'))
        assert error_id == r_json

    def test12_update_comment_id_err(self):
        """
        Test d'erreurs sur la modification d'un commentaire par id
        """
        r = self.app.post(
            '/register',
            data=json.dumps({"user": 'test', "password": 'test'}),
            mimetype='application/json'
        )
        assert r.status_code == 200
        r_json = json.loads(r.data.decode('utf8'))
        token = r_json['token']

        film = random.choice(films)
        comment = random.choice(comments)
        r = self.app.post(
            '/comment',
            data=json.dumps({"token": token, "film": film, "comment": comment}),
            mimetype='application/json'
        )
        assert r.status_code == 200
        r_json = json.loads(r.data.decode('utf8'))
        assert 'film' in r_json
        assert 'comment' in r_json
        assert 'user' in r_json
        assert 'date' in r_json
        assert 'id' in r_json
        assert film == r_json['film']
        assert comment == r_json['comment']
        assert 'test' == r_json['user']
        comment_test = r_json
        id_comment = r_json['id']

        r = self.app.put(
            '/comment/%s' % id_comment,
            data=json.dumps({"token": users['Test1']['token'], "film": 'test', "comment": 'test'})
        )
        assert r.status_code == 400
        r_json = json.loads(r.data.decode('utf8'))
        assert error_json == r_json

        r = self.app.put(
            '/comment/%s' % id_comment,
            data=json.dumps({"film": 'test', "comment": 'test'}),
            mimetype='application/json'
        )
        assert r.status_code == 401
        r_json = json.loads(r.data.decode('utf8'))
        assert error_token == r_json

        r = self.app.put(
            '/comment/%s' % id_comment,
            data=json.dumps({"token": token, "comment": 'test'}),
            mimetype='application/json'
        )
        assert r.status_code == 400
        r_json = json.loads(r.data.decode('utf8'))
        assert error_film == r_json

        r = self.app.put(
            '/comment/%s' % id_comment,
            data=json.dumps({"token": token, "film": 'test'}),
            mimetype='application/json'
        )
        assert r.status_code == 400
        r_json = json.loads(r.data.decode('utf8'))
        assert error_comment == r_json

        r = self.app.put(
            '/comment/%s' % id_comment,
            data=json.dumps({"token": str(uuid.uuid4()), "film": 'test', "comment": 'test'}),
            mimetype='application/json'
        )
        assert r.status_code == 401
        r_json = json.loads(r.data.decode('utf8'))
        assert error_token == r_json

        r = self.app.put(
            '/comment/%s' % str(uuid.uuid4()),
            data=json.dumps({"token": token, "film": 'test', "comment": 'test'}),
            mimetype='application/json'
        )
        assert r.status_code == 400
        r_json = json.loads(r.data.decode('utf8'))
        assert error_id == r_json

    def test13_delete_comment_id_err(self):
        """
        Test d'erreurs sur la suppresion d'un commentaire par id
        """
        r = self.app.post(
            '/register',
            data=json.dumps({"user": 'test', "password": 'test'}),
            mimetype='application/json'
        )
        assert r.status_code == 200
        r_json = json.loads(r.data.decode('utf8'))
        token = r_json['token']

        film = random.choice(films)
        comment = random.choice(comments)
        r = self.app.post(
            '/comment',
            data=json.dumps({"token": token, "film": film, "comment": comment}),
            mimetype='application/json'
        )
        assert r.status_code == 200
        r_json = json.loads(r.data.decode('utf8'))
        assert 'film' in r_json
        assert 'comment' in r_json
        assert 'user' in r_json
        assert 'date' in r_json
        assert 'id' in r_json
        assert film == r_json['film']
        assert comment == r_json['comment']
        assert 'test' == r_json['user']
        comment_test = r_json
        id_comment = r_json['id']

        r = self.app.delete('/comment/%s' % id_comment, data=json.dumps({"token": token}))
        assert r.status_code == 400
        r_json = json.loads(r.data.decode('utf8'))
        assert error_json == r_json

        r = self.app.delete(
            '/comment/%s' % id_comment,
            data=json.dumps({"token": str(uuid.uuid4())}),
            mimetype='application/json'
        )
        assert r.status_code == 401
        r_json = json.loads(r.data.decode('utf8'))
        assert error_token == r_json

        r = self.app.delete(
            '/comment/%s' % str(uuid.uuid4()),
            data=json.dumps({"token": token}),
            mimetype='application/json'
        )
        assert r.status_code == 400
        r_json = json.loads(r.data.decode('utf8'))
        assert error_id == r_json

        r = self.app.post(
            '/register',
            data=json.dumps({"user": 'test2', "password": 'test'}),
            mimetype='application/json'
        )
        assert r.status_code == 200
        r_json = json.loads(r.data.decode('utf8'))
        token = r_json['token']

        r = self.app.delete(
            '/comment/%s' % id_comment,
            data=json.dumps({"token": token}),
            mimetype='application/json'
        )
        assert r.status_code == 401
        r_json = json.loads(r.data.decode('utf8'))
        assert error_token == r_json

if __name__ == '__main__':
    unittest.main()
