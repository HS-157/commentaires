# Commentaires

-----
À tester, pas à utiliser mais à douter !
-----


# Pré-requis

- Python 3
- Flask
- Unittest (test)
- Pylint (test)


## Utilisation

Lancer l'application :

~~~bash
make run
~~~

Lancer les tests :

~~~bash
make test
~~~

Lancer les tests et l'application :

~~~
make
~~~


## API

## ```/```

### GET

~~~
Hello World
~~~


## ```/register```

### POST

Permet de créer un utilisateur.

Entrée :
~~~python
{"user": "user", "password": "XXXX"}
~~~

Sortie :
~~~python
{'token' : "xxxxxx"}
~~~


## ```/login```

### POST

Permet de s'identifier.

Entrée :
~~~python
{"user": "user", "password": "XXXX"}
~~~

Sortie :
~~~python
{'token' : "xxxxxx"}
~~~

## ```/comment```

### POST

Permet de créer un commentaire.

Entrée :
~~~python
{
'film': "Robin des bois"
'comment': "TRO B1§§§"
'token': "xxxxxxxxx"
}
~~~

Sortie :
~~~python
{
'film': "Robin des bois"
'comment': "TRO B1§§§"
'user': "Test"
'date': "2018-01-30 18:09:25"
'id': "xxxxxxxx"
}
~~~

### GET

Récupère tout les commentaires.


## ```/comment/<id>```

### GET

Récupère un commentaires par rapport à son id.

### PUT

Entrée :
~~~python
{
'film': "Robin des bois"
'comment': "Trob bien !"
'token': "xxxxxxxxx"
}
~~~

Sortie :
~~~python
{
'film': "Robin des bois"
'user': "Test"
'comment': "Trob bien !"
'date': "2018-01-30 18:09:25"
'id': "xxxxxxxx"
}
~~~

### DELETE

Supprime un commentaire par rapport à son id.

Entrée :
~~~python
{
'token': "xxxxxxxx"
}
~~~

Sortie :
~~~python
{
'film': "Robin des bois"
'user': "Test"
'comment': "Trob bien !"
'date': "2018-01-30 18:09:25"
'id': "xxxxxxxx"
}
~~~
