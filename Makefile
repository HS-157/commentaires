all: test run

run:
	python main.py

test:
	pylint main.py || exit 0
	pylint test_main.py || exit 0
	python test_main.py
